﻿using Expenses.Api.Models.Expenses;
using Expenses.Data.Model;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Expenses.Service
{
    public interface IExpensesService
    {
        Task<Expense> Create(CreateExpenseModel model);

        Expense Get(int id);

        IQueryable<Expense> Get();

        Task<Expense> Update(int id, UpdateExpenseModel model);

        Task Delete(int id);
    }
}
