﻿using Expenses.Data.Model;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Expenses.Service
{
    public interface IBookService 
    {
        Task<Book> Create(Book model);

        Book Get(int id);

        Task<Book> Update(int id, Book model);

        Task Delete(int id);
    }
}
