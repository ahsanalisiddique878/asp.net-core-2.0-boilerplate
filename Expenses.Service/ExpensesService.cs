﻿using Expenses.Api.Common.Exceptions;
using Expenses.Api.Models.Expenses;
using Expenses.Data.Access.DAL;
using Expenses.Data.Model;
using Expenses.Queries.Queries;
using Expenses.Security;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Expenses.Service
{
    public class ExpensesService : IExpensesService
    {
        private readonly IExpensesQueryProcessor _expensesRepository;
        private readonly IUnitOfWorkRepository unitOfWork;

        public ExpensesService(IExpensesQueryProcessor expensesRepository, IUnitOfWorkRepository unitOfWork)
        {
            _expensesRepository = expensesRepository;
            this.unitOfWork = unitOfWork;
        }

        public async Task<Expense> Create(CreateExpenseModel model)
        {
            var a = await _expensesRepository.Create(model);
            return a;
        }

        public async Task Delete(int id)
        {
            await _expensesRepository.Delete(id);
        }

        public Expense Get(int id)
        {
            var a =  _expensesRepository.Get(id);
            return a;
        }

        public IQueryable<Expense> Get()
        {
            var a = _expensesRepository.Get();
            return a;
        }

        public async Task<Expense> Update(int id, UpdateExpenseModel model)
        {
            var a = await _expensesRepository.Update(id, model);
            return a;
        }
    }
}
