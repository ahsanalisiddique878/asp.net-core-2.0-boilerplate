﻿using Expenses.Api.Common.Exceptions;
using Expenses.Data.Access.DAL;
using Expenses.Data.Model;
using Expenses.Queries.Queries;
using Expenses.Security;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Expenses.Service
{
    public class BookService : IBookService
    {
        private readonly IUnitOfWork _uow;
        private readonly ISecurityContext _securityContext;
        private readonly IBookRepository _bookRepository;

        public BookService(IUnitOfWork uow, ISecurityContext securityContext, IBookRepository bookRepository)
        {
            _uow = uow;
            _securityContext = securityContext;
            _bookRepository = bookRepository;
        }

        public async Task<Book> Create(Book model)
        {
            var a = await _bookRepository.Create(model);
            return a;
        }

        public async Task Delete(int id)
        {
            await _bookRepository.Delete(id);
        }

        public Book Get(int id)
        {
            var a = _bookRepository.Get(id);
            return a;
        }

        public async Task<Book> Update(int id, Book model)
        {
            var a = await _bookRepository.Update(id, model);
            return a;
        }
    }
}
