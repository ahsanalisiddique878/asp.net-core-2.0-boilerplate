﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Expenses.Api.Models.Expenses;
using Expenses.Data.Model;
using Expenses.Filters;
using Expenses.Maps;
using Expenses.Queries.Queries;
using Expenses.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Expenses.Server.RestAPI
{
    [Route("api/[controller]")]
    [Authorize]
    public class BookController : Controller
    {
        private readonly IBookRepository _bookRepo;
        private readonly IAutoMapper _mapper;

        private readonly IBookService BookService;

        public BookController(IBookRepository _bookRepo, IAutoMapper mapper,IBookService bookService)
        {
            this._bookRepo = _bookRepo;
            _mapper = mapper;
            BookService = bookService;
        }

        [HttpGet]
        [QueryableResult]
        public IQueryable<BookModel> Get()
        {
            var result = _bookRepo.Get();
            var models = _mapper.Map<Book, BookModel>(result);
            return models;
        }

        [HttpGet("{id}")]
        public BookModel Get(int id)
        {
            var item = BookService.Get(id);
            var model = _mapper.Map<BookModel>(item);
            return model;
        }

        [HttpPost]
        [ValidateModel]
        public async Task<BookModel> Post([FromBody]Book requestModel)
        {
            var item = await BookService.Create(requestModel);
            var model = _mapper.Map<BookModel>(item);
            return model;
        }

        [HttpPut("{id}")]
        [ValidateModel]
        public async Task<BookModel> Put(int id, [FromBody]Book requestModel)
        {
            var item = await BookService.Update(id, requestModel);
            var model = _mapper.Map<BookModel>(item);
            return model;
        }

        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            await BookService.Delete(id);
        }
    }
}