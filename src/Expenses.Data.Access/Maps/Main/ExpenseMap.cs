﻿using Expenses.Data.Access.Maps.Common;
using Expenses.Data.Model;
using Microsoft.EntityFrameworkCore;

namespace Expenses.Data.Access.Maps.Main
{
    public class BookMap : IMap
    {
        public void Visit(ModelBuilder builder)
        {
            builder.Entity<Book>()
                .ToTable("Books")
                .HasKey(x => x.Id);
        }
    }
}