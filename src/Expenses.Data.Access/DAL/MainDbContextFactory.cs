﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Expenses.Data.Access.DAL
{
    class MainDbContextFactory : IDesignTimeDbContextFactory<MainDbContext>
    {
        MainDbContext IDesignTimeDbContextFactory<MainDbContext>.CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<MainDbContext>();
            builder.UseSqlServer("Server=.;Database=expenses.main;User ID=sa;Password=admin123;TrustServerCertificate=True;Trusted_Connection=False;Connection Timeout=30;Integrated Security=False;Persist Security Info=False;Encrypt=True;MultipleActiveResultSets=True;",
                optionsBuilder => optionsBuilder.MigrationsAssembly(typeof(MainDbContext).GetTypeInfo().Assembly.GetName().Name));

            return new MainDbContext(builder.Options);
        }
    }
}
