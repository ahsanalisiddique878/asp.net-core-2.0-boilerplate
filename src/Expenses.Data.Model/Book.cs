﻿using System;

namespace Expenses.Data.Model
{
    public class Book
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}