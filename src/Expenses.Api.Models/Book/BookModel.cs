﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Expenses.Api.Models.Expenses
{
    public class BookModel
    {
        [Required]
        public string Name { get; set; }

        public int Id { get; set; }
    }
}