﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Expenses.Api.Common.Exceptions;
using Expenses.Api.Models.Common;
using Expenses.Api.Models.Expenses;
using Expenses.Api.Models.Users;
using Expenses.Data.Access.DAL;
using Expenses.Data.Model;
using Expenses.Security;
using Microsoft.EntityFrameworkCore;

namespace Expenses.Queries.Queries
{
    public class UnitOfWorkRepository : IUnitOfWorkRepository
    {
        private readonly IBookRepository bookRepository;
        private readonly IExpensesQueryProcessor expensesQueryProcessor;

        public UnitOfWorkRepository(IBookRepository bookRepository, IExpensesQueryProcessor expensesQueryProcessor)
        {
            this.bookRepository = bookRepository;
            this.expensesQueryProcessor = expensesQueryProcessor;
        }

        public IBookRepository BookRepository
        {
            get
            {
                return this.bookRepository;
            }
        }

        public IExpensesQueryProcessor ExpenseRepository
        {
            get
            {
                return this.expensesQueryProcessor;
            }
        }
    }
}