﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Expenses.Api.Common.Exceptions;
using Expenses.Api.Models.Common;
using Expenses.Api.Models.Expenses;
using Expenses.Api.Models.Users;
using Expenses.Data.Access.DAL;
using Expenses.Data.Model;
using Expenses.Security;
using Microsoft.EntityFrameworkCore;

namespace Expenses.Queries.Queries
{
    public class BookRepository : IBookRepository
    {
        private readonly IUnitOfWork _uow;
        private readonly ISecurityContext _securityContext;

        public BookRepository(IUnitOfWork uow, ISecurityContext securityContext)
        {
            _uow = uow;
            _securityContext = securityContext;
        }

        public IQueryable<Book> Get()
        {
            var query =  GetQuery();
            return query;
        }

        private IQueryable<Book> GetQuery()
        {
            var q = _uow.Query<Book>();

           

            return q;
        }

        public Book Get(int id)
        {
            var user = GetQuery().FirstOrDefault(x => x.Id == id);

            if (user == null)
            {
                throw new NotFoundException("Expense is not found");
            }

            return user;
        }

        public async Task<Book> Create(Book model)
        {
            _uow.Add(model);
            await _uow.CommitAsync();

            return model;
        }

        public async Task<Book> Update(int id, Book model)
        {
            var expense = GetQuery().FirstOrDefault(x => x.Id == id);

            if (expense == null)
            {
                throw new NotFoundException("Expense is not found");
            }

            expense.Name = model.Name;

            await _uow.CommitAsync();
            return expense;
        }

        public async Task Delete(int id)
        {
            var user = GetQuery().FirstOrDefault(u => u.Id == id);
            this._uow.Remove<Book>(user);
            if (user == null)
            {
                throw new NotFoundException("Expense is not found");
            }
            await _uow.CommitAsync();
        }

        
    }
}