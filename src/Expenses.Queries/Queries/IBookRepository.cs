﻿using System.Linq;
using System.Threading.Tasks;
using Expenses.Api.Models.Expenses;
using Expenses.Data.Model;

namespace Expenses.Queries.Queries
{
    public interface IBookRepository
    {
        IQueryable<Book> Get();
        Book Get(int id);
        Task<Book> Create(Book model);
        Task<Book> Update(int id, Book model);
        Task Delete(int id);
    }
}