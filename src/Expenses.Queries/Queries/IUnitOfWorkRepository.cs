﻿using System.Linq;
using System.Threading.Tasks;
using Expenses.Api.Models.Expenses;
using Expenses.Data.Model;

namespace Expenses.Queries.Queries
{
    public interface IUnitOfWorkRepository
    {
        IBookRepository BookRepository { get; }

        IExpensesQueryProcessor ExpenseRepository { get; }
    }
}